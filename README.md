# Celestial Cafe

Welcome to the Celestial Cafe, where we sell delicacies you can only dream about! 
(You may even question if they're real). Choose from an assortment of menu items and customize your order as you see fit. If you're lucky you might qualify for a discount! 

## How to run

### Step 1
Download repository.

### Step 2
Open in VSCode with JavaFX installed.

## Step 3 
Locate the CafeUI class (can be found in celestial_cafe.frontEnd in the source code folder)

## Step 4 
Run CafeUI.
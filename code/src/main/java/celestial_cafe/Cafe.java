package celestial_cafe;

import java.io.*;
import java.util.*;

import celestial_cafe.CafeData.*;
import celestial_cafe.Products.*;

public class Cafe {
    private final static Console console = System.console();

    public static void main( String[] args )
    {
        //ICafeData cafeLoad = new FileCafe();
        //List<MenuItem> menuChoice = cafeLoad.loadProducts("cafeFiles\\MenuFlavour.txt");    

        //List<MenuItem> userCart = new ArrayList<>();

        System.out.println("Welcome to Celestial Cafe!");
        System.out.println("Are you ready to order?");

        String answer = console.readLine("(Y) / (N) ? : ");
        readyOrder(answer);
        int typeMenu = Integer.parseInt(console.readLine("Enter the corresponding number: "));
        
        //BakedGood userBG;
        if (typeMenu == 1) {   
        }
        //List <Drink> drinkOptions = displaySubOptions( menuChoice, "user");
    }

    public static void readyOrder(String ready) {
        if (ready.compareToIgnoreCase("Y") == 0) {
            System.out.println("What would you like to order? \n(1) Drink \n(2) Baked Goods? \n");
        } else{
            System.out.println("Aww too bad. Come back when you're starving");
        }
    }
 
    public static void displayMenuChoice(String choice, List<MenuItem> menu) {

        if (choice.compareToIgnoreCase("Drink") == 0) {
            for(MenuItem i : menu){
                if(i instanceof Drink == true){
                    System.out.println(i);
                }
            }
        } 
        else if (choice.compareToIgnoreCase("Baked Goods") == 0) {
            for(MenuItem i : menu){
                if(i instanceof BakedGood == true){
                    System.out.println(i);
                }
            }
        } 
        else {
            System.out.println("There's such thing as" +choice);
        }    
    } 
    
    public static List<Drink> displaySubOptions(List<MenuItem> menu, String choice) {
        //System.out.println("Select your a type of drink \n(1) Coffee \n(2) Tea \n(3) Milkshake ");
        //int typeChoice = Integer.parseInt(console.readLine("Enter the corresponding number: "));

        List <Drink> drinkOptions = new ArrayList<>();

        for (MenuItem m : menu) {
            if (choice.equalsIgnoreCase("coffee") &&  m instanceof Coffee == true) {
                drinkOptions.add((Drink) m);
                System.out.println(m);
            } 
            else if (choice.equalsIgnoreCase("tea") && m instanceof Tea == true) {
                drinkOptions.add((Drink) m);
                System.out.println(m);
            } 
            else if (choice.equalsIgnoreCase("milkshake") && m instanceof Milkshake == true) {
                drinkOptions.add((Drink) m);
                System.out.println(m);
            }
        }
        return drinkOptions;
    }

    public static void printToppings() {
        
        FileCafe cafeLoad = new FileCafe();
        try {
            List<String> t = cafeLoad.loadFile("cafeFiles\\Toppings.txt");
            for (String i : t) {
                System.out.println(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }  
    }

    public static void drinkOrder(List<Drink> drink) {

        System.out.println("Do you want a cold or hot drink?");
        //String temp = console.readLine();
        //Drink userDrink;
        System.out.println("Select a flavour!");

    }

}

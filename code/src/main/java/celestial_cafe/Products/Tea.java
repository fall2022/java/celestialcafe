package celestial_cafe.Products;

public class Tea extends Drink {

    public Tea (String name, String flavour, double price) {
        super(name, flavour, price);
    }

    @Override
    public String getDisplayName() {
        String s="";
        if (this.isHot) {
            s = this.flavour+" "+this.name;
        } 
        else {
            s = "Iced " + this.flavour+" "+this.name;
        }
        return s;
    }
}

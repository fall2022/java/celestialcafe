package celestial_cafe.Products;

public enum DrinkTopping {
    WHIP("Whipped Cream", 0.5, new String[]{"coffee", "milkshake"}),
    SYRUP("Flavoured Syrup", 0.7, new String[]{"coffee", "tea", "milkshake"}),
    TAPIOCA("Tapioca Pearls",0.6, new String[]{"tea"});

    private final String NAME;
    private final double PRICE;
    private final String[] VALIDDRINKS;
    private boolean compatible;

    private DrinkTopping(String name, double price, String[] drinks){
        this.NAME=name;
        this.PRICE=price;
        this.VALIDDRINKS=drinks;
    }
    public String getName(){
        return this.NAME;
    }
    public double getPrice(){
        return this.PRICE;
    }

    /**
     * This method checks if a drink is compatible with a certain topping 
     * @param drink
     * @return boolean
     */

    /**
     * This method checks if a drink is compatible with a certain topping 
     * @param drink
     * @return compatible
     */
    public boolean isToppingCompatible(Drink drink){
        this.compatible=false;
        for(String s : this.VALIDDRINKS){
            if(drink.getName().equalsIgnoreCase(s)){
                this.compatible=true;

                //toppings are only for cold tea
                if(s.equals("tea")&&drink.isHot){
                    this.compatible=false;
                }
            }
        }
        return this.compatible;
    }
    @Override
    public String toString(){
        return this.NAME+": $"+this.PRICE;
    }

}
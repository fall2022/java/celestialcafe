package celestial_cafe.Products;

public class Coffee extends Drink {

    public Coffee(String name, String flavour, double price) {
        super(name, flavour, price);
    }

    @Override
    public String getDisplayName() {
        String s="";
        if (this.isHot) {
            s = this.flavour;
        } 
        else {
            s = "Iced " + this.flavour;
        }
        return s;
    }
}

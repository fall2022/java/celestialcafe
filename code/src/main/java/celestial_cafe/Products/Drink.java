package celestial_cafe.Products;
import java.util.*;

public abstract class Drink extends MenuItem {

    protected DrinkSize size=DrinkSize.SMALL; 
    protected boolean isHot = true;
    protected List<DrinkTopping> toppings=new ArrayList<>();

    public Drink(String name, String flavour, double price){
        super(name,flavour, price);
    }

    /**
     * Getters
     */
     
    public String getName() {
        return this.name;
    }


    public DrinkSize getSize() {
            return this.size;
    }

    public boolean getIsHot(){
        return this.isHot;
    }    
    

    /**
     * Setters 
     */

    public void setSize(String size) {
        size.toLowerCase();
        switch(size){
            case "small":
            this.size=DrinkSize.SMALL;
            break;
            case "medium":
            this.size=DrinkSize.MEDIUM;
            break;
            case "large":
            this.size=DrinkSize.LARGE;
            break;
        }
    }

    /**
     * User choose if they want a hot or cold drink
     * @param hot
     */
    public void setIsHot(boolean hot) {
        this.isHot = hot;
    }

    @Override
    public void setToppings(List<String> toppings){
        for(String s : toppings){
            s.toLowerCase();
            for(DrinkTopping t : DrinkTopping.values()){
                if(s.equals(t.toString())){
                    this.toppings.add(t);
                }
            }
        }
    }
    @Override
    public List<String> getToppings() {
        List<String> toppingsCopy=new ArrayList<>();
        for(int i=0;i<this.toppings.size();i++){
            toppingsCopy.add(this.toppings.get(i).toString());
        }
        return toppingsCopy;
    }


    @Override
    public double calculateTotalPrice() {
        double toppingsPrice=0;
        for(DrinkTopping t : this.toppings){
            toppingsPrice=toppingsPrice+t.getPrice();
        }
        return this.getPrice()+this.size.getPrice()+toppingsPrice;
    }

}

package celestial_cafe.Products;

public class Milkshake extends Drink {

    public Milkshake(String name, String flavour, double price) {
        super(name, flavour, price);
    }
    @Override
    public String getDisplayName() {
        return this.flavour + " " + this.name;
    }

    //@Override
    public boolean isLactoseFree() {
        return false;
    }

}

package celestial_cafe.Products;

import java.util.*;

public abstract class MenuItem implements Comparable<MenuItem>{

    protected String name;
    protected String type;
    protected double price;
    protected String flavour;
    public List<String> toppings=new ArrayList<>();

    public MenuItem(String name, String flavour, double price) {
        this.name = name;
        this.flavour = flavour;
        this.price = price;
    } 

    //getters:

    public String getName(){
        return this.name;
    }

    public String getFlavour() {
        return this.flavour;
    }

    public double getPrice(){
        return this.price;
    }

    public List<String> getToppings() {
        List<String> toppingsCopy=new ArrayList<>();
        for(int i=0;i<this.toppings.size();i++){
            toppingsCopy.add(this.toppings.get(i));
        }
        return toppingsCopy;
    }

    //setters

    public void setFlavour(String flavour) {
        this.flavour = flavour;
    }


    //abstract methods
    public abstract double calculateTotalPrice();

    public abstract String getDisplayName();

    public abstract void setToppings(List<String> toppings);

    @Override
    public String toString() {
        return "- "+ this.getDisplayName() +" --- $" + this.price ;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MenuItem)) {
            return false;
        }
        MenuItem item = (MenuItem)other;
        if (this.name.equals(item.getName()) && this.flavour.equals(item.getFlavour())) {
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(MenuItem other) {
        if (other.getPrice() == this.price) {
            return 0;
        }
        else if (other.getPrice() > this.price) {
            return -1;
        }
        else {
            return 1;
        }
    }

}

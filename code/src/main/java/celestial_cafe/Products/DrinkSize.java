package celestial_cafe.Products;

public enum DrinkSize {
    SMALL(0, "Small"),
    MEDIUM(0.5, "Medium"),
    LARGE(1.1, "Large");

    private final double PRICE;
    private final String NAME;

    private DrinkSize(double price, String name){
        this.PRICE=price;
        this.NAME=name;
    }

    public double getPrice(){
        return this.PRICE;
    }

    public String getName(){
        return this.NAME;
    }

}

package celestial_cafe.Products;

import java.util.List;

public class BakedGood extends MenuItem{
    
    public BakedGood(String name, String flavour, double price) {
        super(name, flavour, price);
    }

    @Override
    public double calculateTotalPrice() {
        return this.price;
    }
    
    @Override 
    public String getDisplayName(){
        return this.flavour+" "+this.name;
    }
    @Override
    public void setToppings(List<String> toppings) {
        //this code should not run
        toppings.clear();
        this.toppings=toppings;
    }
}

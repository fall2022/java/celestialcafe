package celestial_cafe.CafeData;
import java.util.*;

import celestial_cafe.Products.*;;

public interface ICafeData {
    /**
     * this will create a list of items based on the file information
     * @param fileName
     * @return
     */
    List<MenuItem> loadProducts(String fileName);
}

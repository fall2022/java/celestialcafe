package celestial_cafe.CafeData;

import java.sql.*;
import java.util.*;

import celestial_cafe.Products.*;

public class SqlCafe implements ICafeData {

    private Connection conn;

    public SqlCafe() throws SQLException {
            this.conn = DriverManager.getConnection("jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca");
    }

    @Override 
    public List<MenuItem> loadProducts(String tableName){

        List <MenuItem> lines = new ArrayList<>();
        String QUERY = "SELECT * FROM" + tableName;
        PreparedStatement pstmt = null;

        try {
            conn.setAutoCommit(false);

            pstmt = conn.prepareStatement(QUERY);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                String name = rs.getString("Name");
                String flavour = rs.getString("Flavour");
                Double price = rs.getDouble("Price");
                Drink drink = new Coffee(name, flavour, price);
                lines.add(drink);
            }
            conn.commit();
        } 
        catch (Exception e) {

            try { conn.rollback(); } 
            catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        return lines;
        
    } 
}

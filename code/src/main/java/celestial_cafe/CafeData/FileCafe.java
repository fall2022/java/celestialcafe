package celestial_cafe.CafeData;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import celestial_cafe.Products.*;

public class FileCafe implements ICafeData{

    //protected String fileName = "cafeFiles\\MenuFlavour.txt";
    /***
     * This method takes as input a filename and load it to create the corresponding products
     * @return List<MenuItem>
     */
    @Override
    public List<MenuItem> loadProducts(String fileName) {

        Path path = Paths.get(fileName);
        List<MenuItem> menu = new ArrayList<MenuItem>();
         
        try {
            List <String> lines = Files.readAllLines(path);
            Drink drink;
            BakedGood baked;

            for( String i : lines) {
                final int TYPE_COL = 0;
                final int FLAVOUR_COL = 1;
                final int PRICE_COL = 2;

                String[] col = i.split(",");

                if (col[TYPE_COL].equalsIgnoreCase("Coffee")) {

                    drink = new Coffee(col[TYPE_COL], col[FLAVOUR_COL], Double.parseDouble(col[PRICE_COL]));
                    menu.add(drink);
                } 
                else if (col[TYPE_COL].equalsIgnoreCase("Tea")) {

                    drink = new Tea(col[TYPE_COL], col[FLAVOUR_COL], Double.parseDouble(col[PRICE_COL]));
                    menu.add(drink);
                }
                else if (col[TYPE_COL].equalsIgnoreCase("Milkshake")) {

                    drink = new Milkshake(col[TYPE_COL], col[FLAVOUR_COL], Double.parseDouble(col[PRICE_COL]));
                    menu.add(drink);
                }

                if (col[TYPE_COL].equalsIgnoreCase("Cookies")||
                    col[TYPE_COL].equalsIgnoreCase("Muffin")||
                    col[TYPE_COL].equalsIgnoreCase("Pie")) {

                    baked = new BakedGood(col[TYPE_COL], col[FLAVOUR_COL], Double.parseDouble(col[PRICE_COL]));
                    menu.add(baked);
                }
            }

        }catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return menu;
        
    }

    public List<String> loadFile(String filename) throws IOException {
        Path path = Paths.get(filename);
        return Files.readAllLines(path);

    } 
}
package celestial_cafe.Shopping;

import java.util.*;
import celestial_cafe.Products.*;
import celestial_cafe.CafeData.*;

public class Combos extends Coupon{
    private MenuItem discountItem1;
    private MenuItem discountItem2;

    public Combos() {
        this.type = "Buy one, get one!";
        setRandomDiscountItem();
    }

    @Override
    public boolean isApplicable(List<MenuItem> cartItems) {
        if (cartItems.contains(this.discountItem1) && cartItems.contains(this.discountItem2)) {
            return true;
        }else {
            return false;
        } 
    }

    @Override
    public double calculateDiscount() {
        this.value = discountItem2.getPrice();
        return this.value;
    }

    @Override
    /**
     * Sets a random item in the menu as the coupon's target for eligibility
     */
    public void setRandomDiscountItem(){
        Random random = new Random();
        FileCafe cafeLoad = new FileCafe();
        List<MenuItem> menu = cafeLoad.loadProducts("..\\cafeFiles\\MenuFlavour.txt");

        int itemIndex1 = random.nextInt(menu.size());
        int itemIndex2 = random.nextInt(menu.size());

        this.discountItem1 = menu.get(itemIndex1);
        this.discountItem2 = menu.get(itemIndex2);
    }

    @Override
    public String toString() {
        return "You're eligible for the discount: Buy one " + this.discountItem1.getDisplayName() + ", get one" + this.discountItem2.getDisplayName() + " free!";
    }
    
}

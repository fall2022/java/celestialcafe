package celestial_cafe.Shopping;
import java.util.*;

import celestial_cafe.CafeData.FileCafe;
import celestial_cafe.Products.*;

public abstract class Coupon {
    protected String type;
    protected double value;
    protected MenuItem discountItem;

    /**
     * Returns the value of the coupon
     * @return double
     */
    public double getValue() {
        return this.value;
    }

    /**
     * Returns the type of the coupon
     * @return String
     */
    public String getType() {
        return this.type;
    }

    /**
     * Depending on if coupon is applicable, will return true or false
     * @return boolean
     */
    public abstract boolean isApplicable(List<MenuItem> cartItems);

    /**
     * Calculates the amount to remove from the user's total
     * @return double
     */
    public abstract double calculateDiscount();

    public String toString() {
        return "You're eligible for this "+this.type;
    }

    /**
     * Sets a random item in the menu as the coupon's target for eligibility
     */
    public void setRandomDiscountItem(){
        Random random = new Random();
        FileCafe cafeLoad = new FileCafe();
        List<MenuItem> menu = cafeLoad.loadProducts("..\\cafeFiles\\MenuFlavour.txt");

        int itemIndex = random.nextInt(menu.size());
        this.discountItem = menu.get(itemIndex);
    }
}

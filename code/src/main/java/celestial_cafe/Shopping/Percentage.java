package celestial_cafe.Shopping;
import java.text.DecimalFormat;
import java.util.*;
import celestial_cafe.Products.*;

public class Percentage extends Coupon {

    /**
     * Constructor
     */
    public Percentage() {
        this.type = "percentage";
        setRandomDiscountItem();
    }
    
    /**
     * Checks if the coupon is applicable by receiving the items in cart
     */
    public boolean isApplicable(List<MenuItem> cartItems) {
        if (cartItems.contains(this.discountItem)) {
                return true;
        }
        return false;
    }

    /**
     * Calculates how much the discount will be
     */
    public double calculateDiscount() {
        DecimalFormat df= new DecimalFormat("#.00");
        this.value = this.discountItem.getPrice()*0.30;
        String s=df.format(this.value);
        this.value= Double.parseDouble(s);
        return this.value;
    }

    public String toString() {
        return "Discount of type " + this.type + " of $" + this.calculateDiscount() + " on " + this.discountItem.getDisplayName(); 
    }
}

package celestial_cafe.Shopping;

import java.util.*;

import celestial_cafe.Products.*;

public class SetAmount extends Coupon{
    private Random randomPrice= new Random();
    private boolean applicable;

    public SetAmount(){
        this.type="set amount";
        setRandomDiscountItem();
    }

    @Override
    public boolean isApplicable(List<MenuItem> cartItems) {
        if(cartItems.contains(this.discountItem)){
            this.applicable=true;
        }
        else{
            this.applicable=false;
        }
        
        return this.applicable;
    }

    @Override
    public double calculateDiscount() {
        this.value=0;
        if(this.applicable){
            //this ensures that the discount is not more than the item price
            double itemPrice=this.discountItem.getPrice();
            double discount=this.randomPrice.nextInt((int)itemPrice-1)+1;
            this.value=discount;
        }
        return this.value;
    }

    @Override 
    public String toString(){
        String s="";
        if(this.applicable){
            s="You're eligible for the discount: " + this.type + " of $"+this.calculateDiscount()+" off on the item" + this.discountItem.getDisplayName();
        }
        return s;
    }

}
package celestial_cafe.Shopping;

import java.util.*;
import celestial_cafe.Products.*;

public class ShoppingCart {
   protected int cartId;
   protected List<MenuItem> cartItems = new ArrayList<>();
   protected double total;
   protected Coupon coupon;

    /**
    * Constructor
    * @param cartId
    */
    public ShoppingCart(int cartId) {
        this.cartId = cartId;
    }

    /**
     * @return List<MenuItem> cartItems
     */
    public List<MenuItem> getCartItems() {
        return this.cartItems;
    }

    /**
     * Returns appplicable coupon
     * @return
     */
    public Coupon getCoupon() {
        return this.coupon;
    }

    /**
     * Adds MenuItems to cartItems private field
     * @param item
     */
    public void addItems(MenuItem item) {
        this.cartItems.add(item);
    }

    /**
     * Edits the flavour of a MenuItems in cartItems
     * @param MenuItem item
     * @param String newFlavour
     */
    public void editFlavour(MenuItem item, String newFlavour) {
        int index = this.cartItems.indexOf(item);
        this.cartItems.get(index).setFlavour(newFlavour);
    }

    /**
     * Edits the toppings of a MenuItems in cartItems
     * @param MenuItem item
     * @param List<String> toppingChoices
     */
    public void editToppings(MenuItem item, List<String> toppingChoices) {
        int index = this.cartItems.indexOf(item);
        this.cartItems.get(index).setToppings(toppingChoices);
    }

    /**
     * Edits the size of a MenuItems in cartItems
     * @param MenuItem item
     * @param String newSize
     */
    public void editSize(MenuItem item, String newSize) {
        int index = this.cartItems.indexOf(item);
        if (this.cartItems.get(index) instanceof Drink) {
            ((Drink)this.cartItems.get(index)).setSize(newSize);
        }
    }

    /**
     * Removes a MenuItems from cartItems
     * @param item
     */
    public void removeItem(MenuItem item) {
        this.cartItems.remove(item);
    }

    /**
     * Calculates the total price of all the MenuItems in cartItems
     * @return double
     */
    public double totalCart() {
        for (int i = 0; i < this.cartItems.size(); i++) {
            this.total += this.cartItems.get(i).calculateTotalPrice();
        }
        //adding the tax
        return this.total*1.15;
    }

    /**
     * Prints out all the MenuItems in cartItems
     */
    public void displayCart() {
        for (int i = 0; i < this.cartItems.size(); i++) {
            System.out.println(this.cartItems.get(i));
        }
    }

    public void checkout() {
        //end process
    }

    /**
     * Checks if the customer is eligible for any coupon and sets field for Coupon accordingly
     */
    public void setCoupons() {
        int validCoupons = 0;
        //first boolean for percentageCoupon, second for combosCoupon, third for setAmountCoupon
        boolean[] eligible = new boolean[]{false, false, false};

        Coupon percentageCoupon = new Percentage();
        Coupon combosCoupon = new Combos();
        Coupon setAmountCoupon = new SetAmount();

        if (percentageCoupon.isApplicable(this.cartItems)) {
            percentageCoupon.calculateDiscount();
            eligible[0] = true;
            validCoupons++;
        }
        else if (combosCoupon.isApplicable(this.cartItems)) {
            combosCoupon.calculateDiscount();
            eligible[1] = true;
            validCoupons++;
        }
        else if (setAmountCoupon.isApplicable(this.cartItems)) {
            setAmountCoupon.calculateDiscount();
            eligible[2] = true;
            validCoupons++;
        }

        Random random = new Random();
        if (validCoupons > 1) {
            //temp, fix later to consider if its 2 or 3 not just 3
            int couponChoice = random.nextInt(3);
            eligible[0] = false;
            eligible[1] = false;
            eligible[2] = false;
            eligible[couponChoice] = true;
        }
        
        if (eligible[0]) {
            this.coupon = percentageCoupon;
        }
        else if (eligible[1]) {
            this.coupon = combosCoupon;
        }
        else if (eligible[2]) {
            this.coupon = setAmountCoupon;
        }

    }
}

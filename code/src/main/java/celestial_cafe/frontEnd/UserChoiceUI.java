package celestial_cafe.frontEnd;

import javafx.scene.paint.*;
import javafx.scene.layout.*;
import javafx.scene.text.*;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.*;
import javafx.event.*;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

import java.util.*;

import celestial_cafe.Cafe;
import celestial_cafe.Products.*;
import celestial_cafe.Products.MenuItem;

public class UserChoiceUI implements EventHandler<ActionEvent>{

    protected VBox menuLayout;
    protected VBox maincontainer = new VBox();
    protected VBox drinkSelectionLayout;
    protected String drinkChoice;
    private List<MenuItem> menu;

    protected Drink newUserDrink;
    protected BakedGood bakedGood;

    public UserChoiceUI(List<MenuItem> menu, String drinkChoice, VBox layout) {
       this.drinkChoice = drinkChoice; 
       this.menu = menu;
       this.menuLayout = layout;
    }

    @Override
    public void handle(ActionEvent event) {
        VBox drinkVbox = displayDrinkList();
        this.menuLayout.getChildren().add(drinkVbox);
        this.menuLayout.getChildren().add(getDrinkSelectionLayout());
    }

    public VBox displayDrinkList() {

        //label
        Label drinkLabel = new Label(drinkChoice + " Menu");
        drinkLabel.setFont(Font.font("Brush Script MT", FontWeight.BOLD, 40));
        Text menuText = new Text("Select an option: ");
        menuText.setFont(Font.font("Lucinda Console", FontWeight.NORMAL, 15));

        List<Drink> drinkOptions = Cafe.displaySubOptions(this.menu, this.drinkChoice);
        //Creating an Observablelist
        ObservableList<Drink> drinksList = FXCollections.observableList(drinkOptions);
        ListView <Drink> listView = new ListView<Drink>(drinksList);
        
        //event for the list view selection
        listView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Drink d = listView.getSelectionModel().getSelectedItem();
                //chooseDrink(d);
                setDrinkSelectionLayout(chooseDrink(d));
            }
        }); 

        VBox vbox = new VBox();
        HBox listDrinkBox = new HBox();
        listDrinkBox.setMinHeight(150);
        listDrinkBox.setAlignment(Pos.CENTER);
        listDrinkBox.getChildren().addAll(listView);

        vbox.getChildren().addAll(drinkLabel, menuText, listDrinkBox);
        BackgroundFill bgVbox = new BackgroundFill(Color.rgb(205, 246, 255), CornerRadii.EMPTY, Insets.EMPTY);
        Border borderStyle = new Border(new BorderStroke(Color.BLUE, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1)));
        vbox.setBackground(new Background(bgVbox));
        vbox.setSpacing(10);
        vbox.setPadding(new Insets(40, 20, 50, 20));
        vbox.setAlignment(Pos.CENTER);
        vbox.setBorder(borderStyle);
    
        return vbox;
    }
    
    /**
     * Displays toppings and user chooses
     * @return List<String> 
     */
    public List<String> chooseToppings(Drink select) {
        List<String> choices=new ArrayList<>();

        for(DrinkTopping t: DrinkTopping.values()){
            if(t.isToppingCompatible(select)){
                choices.add(t.toString());
            }
        }
        if(choices.size() == 0){
            choices.add("No toppings available for this drink.");
        }
        return choices;
    }

    public VBox chooseDrink (Drink d) { 

        Drink selectDrink = d;
        VBox drinkSelectBox = new VBox();
       
        //Choosing temperature
        //label for temp
        Label tempLabel = new Label(selectDrink.getDisplayName() + " Temperature:");
        Text tempText = new Text("Would you like a hot or cold " +selectDrink.getName() + "?");
        
        //toggleGroup and radio buttons for temperature
        ToggleGroup tg = new ToggleGroup();
        RadioButton hotBtn = new RadioButton("Hot");
        RadioButton coldBtn = new RadioButton("Cold");
        hotBtn.setToggleGroup(tg);
        coldBtn.setToggleGroup(tg);

        //adding them together
        HBox tempHboxOpt = new HBox(hotBtn, coldBtn);
        VBox tempBox = new VBox(tempLabel, tempText, tempHboxOpt);
        
        //eventListener on tg groupt
        tg.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {

            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                RadioButton selectedBtn = (RadioButton)tg.getSelectedToggle();      
                if (selectedBtn.getText().equalsIgnoreCase("Hot")) {
                    selectDrink.setIsHot(true);
                }
                else {
                    selectDrink.setIsHot(false);
                }
            }       
        });
        
        //Choosing toppings
        TilePane pane = new TilePane();
        Label toppingsLabel = new Label(selectDrink.getDisplayName() + " Toppings:");
        
        Text toppingsText = new Text("What toppings would you like to add to " +selectDrink.getName() + "?");
       

        List<String> listToppings = chooseToppings(selectDrink);
        List<String> selectedToppings = new ArrayList<>();
        //checks for temperature for tea toppings
        tg.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                RadioButton selectedBtn = (RadioButton)tg.getSelectedToggle();      
                if (selectedBtn.getText().equalsIgnoreCase("Cold")) {
                    listToppings.clear();
                    listToppings.addAll(chooseToppings(selectDrink));
                }
                else{
                    listToppings.clear();
                    listToppings.addAll(chooseToppings(selectDrink));
                }
            }       
        });
            

        for (int i = 0; i < listToppings.size(); i++) {
            CheckBox c = new CheckBox(listToppings.get(i));

            int index = i;
            EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {
                public void handle(ActionEvent e) {
                    if (c.isSelected()) {
                        selectedToppings.add(listToppings.get(index));
                    }
                    else {
                        selectedToppings.remove(index);
                    }
                }
            };

            c.setOnAction(event);
            pane.getChildren().add(c);
            pane.setAlignment(Pos.CENTER);
            c.setIndeterminate(true);
        }
        selectDrink.setToppings(selectedToppings);
        
        HBox toppingsOptionsBox = new HBox(pane);
        VBox toppingsBox = new VBox(toppingsLabel, toppingsText, toppingsOptionsBox);
        

        //Choosing drink size

        Label sizeLabel = new Label(selectDrink.getDisplayName() + " Size:");
        Text sizeText = new Text("What size of " + selectDrink.getName() + " would you like?");
        
        //toggleGroup and radio buttons for sizes
        ToggleGroup sizeTg = new ToggleGroup();
        RadioButton smallBtn = new RadioButton("Small");
        RadioButton mediumBtn = new RadioButton("Medium");
        RadioButton largeBtn = new RadioButton("Large");
        smallBtn.setToggleGroup(sizeTg);
        mediumBtn.setToggleGroup(sizeTg);
        largeBtn.setToggleGroup(sizeTg);

        //adding them together
        HBox sizeOptionsHbox = new HBox(smallBtn, mediumBtn, largeBtn);
        VBox sizeBox = new VBox(sizeLabel, sizeText, sizeOptionsHbox);
        

        //eventListener for sizeTg ToggleGroup
        sizeTg.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {

            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                RadioButton selectedBtn = (RadioButton)sizeTg.getSelectedToggle();      
                if (selectedBtn.getText().equalsIgnoreCase("small")) {
                    selectDrink.setSize("small");
                }
                else if (selectedBtn.getText().equalsIgnoreCase("medium")) {
                    selectDrink.setSize("medium");
                }
                else {
                    selectDrink.setSize("large");
                }
            }       
        });

        //setting the drink field
        
        Button addCart = new Button("Add to Cart");
        Label cartText = new Label();
        addCart.setOnAction((e) -> {
            this.newUserDrink = selectDrink;
            String add = "A "+ selectDrink.getSize().getName() + " " + selectDrink.getDisplayName() + " has been added to your cart. Your " +selectDrink.getName()+ "'s total is: $" +selectDrink.calculateTotalPrice();
            cartText.setText(add);
            System.out.println(add);

        });

        //checking if it's a milkshake so the temperature selection isnt shown

        if (d.getName().equalsIgnoreCase("Milkshake")) {
            drinkSelectBox.getChildren().addAll(toppingsBox, sizeBox, addCart, cartText);
        } 
        else {
            drinkSelectBox.getChildren().addAll(tempBox, toppingsBox, sizeBox, addCart, cartText);
        }

        /*
         * Styling values
         */
        Font labelFont = Font.font("Lucida Console", FontWeight.BOLD, 16);
        Font textFont = Font.font("Lucida Console", FontWeight.NORMAL, 14);
        Border borderStyle = new Border(new BorderStroke(Color.BLUE, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1)));
        Insets paddingStyle = new Insets(10, 20, 10, 20);
        Background bgColor = new Background(new BackgroundFill(Color.WHITESMOKE, CornerRadii.EMPTY, Insets.EMPTY));

        /*
         * Styling the font 
         */
        tempLabel.setFont(labelFont);
        tempText.setFont(textFont);
        toppingsLabel.setFont(labelFont);
        toppingsText.setFont(textFont);
        sizeLabel.setFont(labelFont);
        sizeText.setFont(textFont);
        cartText.setFont(labelFont);


        /*
         * Styling every boxes 
         */

         //temperature boxes
        tempBox.setSpacing(10);
        tempBox.setAlignment(Pos.CENTER);
        tempBox.setBorder(borderStyle);
        tempBox.setPadding(paddingStyle);
        tempBox.setBackground(bgColor);
        tempBox.setMaxWidth(600);

        tempHboxOpt.setSpacing(10);
        tempHboxOpt.setAlignment(Pos.CENTER);
        //topping boxes
        toppingsBox.setSpacing(10);
        toppingsBox.setAlignment(Pos.CENTER);
        toppingsBox.setBorder(borderStyle);
        toppingsBox.setPadding(paddingStyle);
        toppingsBox.setBackground(bgColor);
        toppingsBox.setMaxWidth(600);

        toppingsOptionsBox.setSpacing(10);
        toppingsOptionsBox.setAlignment(Pos.CENTER);

        //size boxes
        sizeBox.setSpacing(10);
        sizeBox.setAlignment(Pos.CENTER);
        sizeBox.setBorder(borderStyle);
        sizeBox.setPadding(paddingStyle);
        sizeBox.setBackground(bgColor);
        sizeBox.setMaxWidth(600);

        sizeOptionsHbox.setSpacing(10);
        sizeOptionsHbox.setAlignment(Pos.CENTER);

        //button
        addCart.setPadding(paddingStyle);
        addCart.setMaxSize(170, 30);
        //other boxes
        drinkSelectBox.setSpacing(30);
        drinkSelectBox.setAlignment(Pos.CENTER);
        drinkSelectBox.setPadding(new Insets(40, 20, 30, 20));
        drinkSelectBox.setBackground(new Background(new BackgroundFill(Color.rgb(224, 255, 208), CornerRadii.EMPTY, Insets.EMPTY)));
        drinkSelectBox.setBorder(borderStyle);

        this.menuLayout.getChildren().add(drinkSelectBox);
        this.menuLayout.setSpacing(15);
        this.menuLayout.setPadding(paddingStyle);
        
        clearDrinkSelectionLayout();
        this.drinkSelectionLayout = drinkSelectBox;
        return drinkSelectBox;
    }

    public Drink getNewUserDrink() {
        System.out.println(newUserDrink);
        return newUserDrink;
    }

    public void clearDrinkSelectionLayout() {
        this.drinkSelectionLayout.getChildren().clear();
    }

    public void setDrinkSelectionLayout(VBox drinkSelectionLayout) {
        this.drinkSelectionLayout = drinkSelectionLayout;
    }

    public VBox getDrinkSelectionLayout() {
        return this.drinkSelectionLayout;
    }
}

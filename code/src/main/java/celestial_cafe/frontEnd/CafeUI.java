package celestial_cafe.frontEnd;
import java.util.*;

import celestial_cafe.Products.MenuItem;
import celestial_cafe.Products.*;
import celestial_cafe.Shopping.ShoppingCart;
import celestial_cafe.CafeData.*;
import javafx.application.*;
import javafx.event.*;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.paint.*;
import javafx.scene.text.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.stage.*;

public class CafeUI extends Application{
    private ShoppingCart cart = new ShoppingCart(1); //hard coding for now

    @Override
    public void start(Stage stage){

        //Layouts
        VBox welcomeLayout = new VBox();
        VBox menuLayout = new VBox();
        VBox cartLayout = new VBox();
        //group root
        Group root = new Group();

    
        //Scenes styling
        Scene startScene = new Scene(root, 600, 600); 
        //Scene menuScene = new Scene(menuLayout, 600, 600);
        Scene cartScene = new Scene(cartLayout, 600, 600);
        startScene.setFill(Color.PINK);
        BackgroundFill bgVbox = new BackgroundFill(Color.PINK, CornerRadii.EMPTY, Insets.EMPTY);
        menuLayout.setBackground(new Background(bgVbox));
        cartLayout.setBackground(new Background(bgVbox));
        menuLayout.setAlignment(Pos.CENTER);
        cartLayout.setAlignment(Pos.CENTER);

        //Start options
        HBox startOptions = new HBox();
        startOptions.setAlignment(Pos.CENTER);

        Button backBtn = new Button("Back");
        
        Button menuBtn = new Button("Menu");
        menuBtn.setOnAction((e) -> {
            VBox newMenuLayout = setMenuLayout(menuLayout);
            newMenuLayout.getChildren().add(backBtn);
            Scene menuScene = new Scene(newMenuLayout, 600, 600);
            stage.setScene(menuScene);
        });

        Button cartBtn = new Button("Cart");
        cartBtn.setOnAction((e) -> {
            setCartLayout(cartLayout).getChildren().add(backBtn);
            stage.setScene(cartScene);
        });
        //events
        backBtn.setOnAction(e -> stage.setScene(startScene));

        startOptions.getChildren().addAll(menuBtn, cartBtn);
        startOptions.setMinWidth(400);
        startOptions.setAlignment(Pos.CENTER);
        startOptions.setSpacing(20);
        startOptions.setPadding(new Insets(10, 10, 10, 10));

        //style
        menuBtn.setMinWidth(150);
        cartBtn.setMinWidth(150);
        Text welcomeMsg = new Text("Welcome to Celestial Cafe!");
        Font textFont = Font.font("Brush Script MT", FontWeight.BOLD, 50);
        welcomeMsg.setFont(textFont);

        welcomeLayout.getChildren().addAll(welcomeMsg,startOptions);
        welcomeLayout.setAlignment(Pos.CENTER);
        welcomeLayout.setPadding(new Insets(20, 20, 20, 20));

        root.getChildren().addAll(welcomeLayout);
        root.setLayoutX(0);

        stage.setTitle("Welcome to Celestial Cafe!"); 
        stage.setMinWidth(300);
        //Adding scene to stage and display the stage

        stage.setScene(startScene); 
        stage.show();  
    }

    public static void main(String[] args) {
        Application.launch(args);
    }

    public static VBox setCartLayout(VBox layout) {
        //actions user can take in cart 
        HBox actionOptions = new HBox();
        Button editFlavourBtn = new Button("Edit flavour");
        Button editToppingsBtn = new Button("Edit toppings");
        Button editSizeBtn = new Button("Edit size");
        Button removeBtn = new Button("Remove");

        //display drinks
        
        //get drink choice for action
        //temp
        Drink drinkChoice = new Tea("Tea","Matcha", 5);

        //events
        ShoppingCartUI editFlavourChoice = new ShoppingCartUI(drinkChoice, editFlavourBtn.getText(), layout);
        editFlavourBtn.addEventHandler(ActionEvent.ACTION, editFlavourChoice);

        ShoppingCartUI editToppingChoice = new ShoppingCartUI(drinkChoice, editToppingsBtn.getText(), layout);
        editToppingsBtn.addEventHandler(ActionEvent.ACTION, editToppingChoice);

        ShoppingCartUI editSizeChoice = new ShoppingCartUI(drinkChoice, editSizeBtn.getText(), layout);
        editSizeBtn.addEventHandler(ActionEvent.ACTION, editSizeChoice);

        ShoppingCartUI removeChoice = new ShoppingCartUI(drinkChoice, removeBtn.getText(), layout);
        removeBtn.addEventHandler(ActionEvent.ACTION, removeChoice);

        actionOptions.getChildren().addAll(editFlavourBtn, editToppingsBtn, editSizeBtn, removeBtn);
        layout.getChildren().add(actionOptions);

        //style
        editFlavourBtn.setMinWidth(120);
        editToppingsBtn.setMinWidth(120);
        editSizeBtn.setMinWidth(120);
        removeBtn.setMinWidth(120);

        layout.setAlignment(Pos.CENTER);
        actionOptions.setSpacing(20);
        actionOptions.setMinWidth(400);

        return layout;

    }

    public static VBox setMenuLayout(VBox layout){
        //drink options 
        HBox drinkOptions = new HBox();
        drinkOptions.setAlignment(Pos.CENTER);

        Button coffeeButton = new Button("Coffee");
        Button teaButton = new Button("Tea");
        Button milkshakeButton = new Button("Milkshake");

        //events
        ICafeData cafeLoad = new FileCafe();
        List<MenuItem> menuChoice = cafeLoad.loadProducts("cafeFiles\\MenuFlavour.txt");   

        UserChoiceUI coffeeChoice = new UserChoiceUI(menuChoice, coffeeButton.getText(), layout);
        coffeeButton.addEventHandler(ActionEvent.ACTION , coffeeChoice); 
        
        UserChoiceUI teaChoice = new UserChoiceUI(menuChoice, teaButton.getText(), layout);
        teaButton.addEventHandler(ActionEvent.ACTION , teaChoice);

        UserChoiceUI milkshakeChoice = new UserChoiceUI(menuChoice, milkshakeButton.getText(), layout);
        milkshakeButton.addEventHandler(ActionEvent.ACTION, milkshakeChoice);
        
        drinkOptions.getChildren().addAll(coffeeButton, teaButton, milkshakeButton);
        layout.getChildren().add(drinkOptions);

        //style
        coffeeButton.setMinWidth(150);
        teaButton.setMinWidth(150);
        milkshakeButton.setMinWidth(150);

        drinkOptions.setPadding(new Insets(10, 10, 10, 10));
        drinkOptions.setSpacing(20);
        drinkOptions.setMinWidth(400);

        layout.setAlignment(Pos.CENTER);
        layout.setPadding(new Insets(10, 10, 10, 10));
        layout.setSpacing(20);
        

        return layout;
    }

    public void displayCoupon(List<MenuItem> cartItems, Group root) { //user was really thirsty and just ordered everything
        for (MenuItem item : cartItems) {
            this.cart.addItems(item);
        }
        this.cart.setCoupons();
        
        HBox couponContainer = new HBox();
        TextField coupon = new TextField();

        if (this.cart.getCoupon() != null) {
            coupon.setText(cart.getCoupon().toString());
        }
        else {
            coupon.setText("No coupon applicable! :(");
        }

        couponContainer.getChildren().add(coupon);
        root.getChildren().add(couponContainer);
    }

    public static void addDrinkToCart(Drink d) {
        System.out.println(d);
    }
}

package celestial_cafe.frontEnd;

import celestial_cafe.Shopping.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.util.*;

import celestial_cafe.CafeData.*;
import celestial_cafe.Products.*;
import celestial_cafe.Products.MenuItem;

public class ShoppingCartUI implements EventHandler<ActionEvent>{
    
    private Drink drinkChoice;
    private String actionChoice;
    private ShoppingCart cart;
    private VBox layout;

    public ShoppingCartUI(Drink drinkChoice, String actionChoice, VBox layout) {
        this.drinkChoice = drinkChoice;
        this.actionChoice = actionChoice;
        cart = new ShoppingCart(1); //hard coded cart id for now
        this.layout = layout;
    }

    @Override
    public void handle(ActionEvent event) {
        if (this.actionChoice.equals("edit flavour")) {
            String newFlavour = chooseFlavour();
            cart.editFlavour(this.drinkChoice, newFlavour);
        }
        else if (this.actionChoice.equals("edit toppings")) {
            List<String> newToppings = chooseToppings();
            cart.editToppings(this.drinkChoice, newToppings);
        }
        else if (this.actionChoice.equals("edit size")) {
            String newSize = chooseSize();
            cart.editSize(this.drinkChoice, newSize);
        }
        else if (this.actionChoice.equals("remove")) {
            cart.removeItem(this.drinkChoice);
        }
    }

    /**
     * Displays flavours and user chooses
     * @return String
     */
    public String chooseFlavour() {
        ICafeData cafeLoad = new FileCafe();
        List<MenuItem> menuChoice = cafeLoad.loadProducts("cafeFiles\\MenuFlavour.txt");  
        //using method in user choice to show flavours to user
        //UserChoice choice = new UserChoice(menuChoice, this.drinkChoice.getName(), this.root);
        //VBox drinkChoices = choice.displayDrinkList();
        //this.root.getChildren().add(drinkChoices);

        //call method that gets what user selected

        //temp return statement
        return "";
    }

    /**
     * Displays toppings and user chooses
     * @return List<String> 
     */
    public List<String> chooseToppings() {
        List<String> choices=new ArrayList<>();

        for(DrinkTopping t: DrinkTopping.values()){
            if(t.isToppingCompatible(drinkChoice)){
                choices.add(t.toString());
            }
        }
        if(choices.size()==0){
            choices.add("No toppings available for this drink.");
        }
        return choices;
    }

    /**
     * Displays sizes and user chooses
     * @return String
     */
    public String chooseSize() {
        List<DrinkSize> sizes = new ArrayList();
        sizes.add(DrinkSize.SMALL);
        sizes.add(DrinkSize.MEDIUM);
        sizes.add(DrinkSize.LARGE);

        ObservableList<DrinkSize> sizesList = FXCollections.observableList(sizes);
        ListView <DrinkSize> listView = new ListView<DrinkSize>(sizesList);

        VBox sizesContainer = new VBox();
        sizesContainer.getChildren().add(listView);
        this.layout.getChildren().add(sizesContainer);

        //method to get what user selected 
        //temp return statement 
        return "";
    }
}

package celestial_cafe;
import java.util.*;
import celestial_cafe.Products.*;
import celestial_cafe.Shopping.*;
import static org.junit.Assert.*;
import org.junit.Test;


public class TestShoppingCart {
    private Drink tea = new Tea("Tea","Matcha", 5);
    private ShoppingCart cart = new ShoppingCart(1);

    @Test
    public void testAddItems() {
        this.cart.addItems(tea);
        assertEquals(this.tea, cart.getCartItems().get(0));
    }
    @Test
    public void testEditFlavour() {
        this.cart.addItems(this.tea);
        this.cart.editFlavour(this.tea, "Chai");
        Drink newTea = new Tea("Tea","Chai", 5);
        assertEquals(newTea, cart.getCartItems().get(0)); 
    }
    @Test
    public void testEditToppings() {
        this.tea.setIsHot(false);
        this.cart.addItems(this.tea);
        List<String> toppings = new ArrayList<String>();
        toppings.add("Flavoured Syrup: $0.7");
        this.cart.editToppings(this.tea, toppings);
        assertEquals(toppings.get(0), this.cart.getCartItems().get(0).getToppings().get(0));
    }
    @Test
    public void testEditSize() {
        this.cart.addItems(this.tea);
        this.cart.editSize(tea, "small");
        Drink tea = (Drink)this.cart.getCartItems().get(0);
        assertEquals(DrinkSize.SMALL, tea.getSize());
    }
    @Test
    public void testRemoveItem() {
        Drink newTea = new Tea("Tea","Chai", 5);
        this.cart.addItems(this.tea);
        this.cart.addItems(newTea);
        this.cart.removeItem(this.tea);
        assertEquals(1, this.cart.getCartItems().size());
    }

}

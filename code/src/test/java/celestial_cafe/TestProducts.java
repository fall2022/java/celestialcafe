package celestial_cafe;
import celestial_cafe.Products.*;
import static org.junit.Assert.*;
import org.junit.Test;
import java.util.*;

public class TestProducts {
    private Drink tea = new Tea("Tea","Matcha", 5);
    private Drink coffee = new Coffee("Coffee","Cappuccino", 4);
    private Drink milkshake = new Milkshake("Milkshake","Chocolate", 3);
    private List<String> toppings= new ArrayList<>();
    private final double TOLERANCE=0.000001;
    

    @Test
    public void testConstructorFields(){
        assertEquals("Tea",this.tea.getName());
        assertEquals("Coffee",this.coffee.getName());
        assertEquals("Milkshake",this.milkshake.getName());

        assertEquals("Matcha", this.tea.getFlavour());
        assertEquals("Cappuccino", this.coffee.getFlavour());
        assertEquals("Chocolate", this.milkshake.getFlavour());

        assertEquals(5,this.tea.getPrice(),this.TOLERANCE);
        assertEquals(4,this.coffee.getPrice(),this.TOLERANCE);
        assertEquals(3,this.milkshake.getPrice(),this.TOLERANCE);
    }
    @Test 
    public void testGetDisplayName(){
        assertEquals("Cappuccino", this.coffee.getDisplayName());
        //assertEquals("Matcha Tea", this.tea.getDisplayName());
        //assertEquals("Chocolate Milkshake", this.tea.getDisplayName());
    }

    @Test
    public void testSetIsHot(){
        this.coffee.setIsHot(false);
        this.tea.setIsHot(false);
        this.milkshake.setIsHot(true);
        assertEquals("Iced Cappuccino", this.coffee.getDisplayName());
        assertEquals("Iced Matcha Tea",this.tea.getDisplayName());
        assertEquals("Chocolate Milkshake",this.milkshake.getDisplayName());

        this.coffee.setIsHot(true);
        this.tea.setIsHot(true);
        this.milkshake.setIsHot(false);

        assertEquals("Cappuccino", this.coffee.getDisplayName());
        assertEquals("Matcha Tea",this.tea.getDisplayName());
        assertEquals("Chocolate Milkshake",this.milkshake.getDisplayName());

    }
    @Test
    public void testToString(){
        assertEquals("- Cappuccino --- $4.0",this.coffee.toString());
        assertEquals("- Matcha Tea --- $5.0",this.tea.toString());
        assertEquals("- Chocolate Milkshake --- $3.0",this.milkshake.toString());
    }
    @Test
    public void testCompare() {
        MenuItem item = new Tea("Tea","Matcha", 5);;
        assertEquals(this.tea.equals(item), true);
        assertEquals(this.tea.compareTo(this.coffee), 1, TOLERANCE);
    }
    @Test
    public void testSorting(){
        List <MenuItem> test = new ArrayList<>();
        test.add(this.tea);
        test.add(this.milkshake);
        test.add(this.coffee);

        Collections.sort(test);
        assertEquals(milkshake, test.get(0));
        assertEquals(coffee, test.get(1));
        assertEquals(tea, test.get(2));

    }
    @Test
    public void testSizePrices(){
        assertEquals(0.0, DrinkSize.SMALL.getPrice(), TOLERANCE);
        assertEquals(0.5, DrinkSize.MEDIUM.getPrice(), TOLERANCE);
        assertEquals(1.1, DrinkSize.LARGE.getPrice(), TOLERANCE);
    }
    @Test
    public void testGetTotalPrice(){
        coffee.setSize("small");
        assertEquals(4.0, coffee.calculateTotalPrice(),TOLERANCE);
        coffee.setSize("medium");
        assertEquals(4.5, coffee.calculateTotalPrice(),TOLERANCE);
        coffee.setSize("large");
        assertEquals(5.1, coffee.calculateTotalPrice(),TOLERANCE);
    }
    @Test
    public void testDrinkTopping(){
        DrinkTopping whipCream=DrinkTopping.WHIP;
        assertEquals("Whipped Cream",whipCream.getName());
        assertEquals(0.5, whipCream.getPrice(), TOLERANCE);
        assertTrue(whipCream.isToppingCompatible(this.coffee));
        assertTrue(whipCream.isToppingCompatible(this.milkshake));
        assertFalse(whipCream.isToppingCompatible(this.tea));

        DrinkTopping boba=DrinkTopping.TAPIOCA;
        this.tea.setIsHot(false);
        assertTrue(boba.isToppingCompatible(this.tea));
        this.tea.setIsHot(true);
        assertFalse(boba.isToppingCompatible(this.tea));
    }
    @Test
    public void testSetToppings(){
        
        toppings.add("Whipped Cream: $0.5");
        toppings.add("Flavoured Syrup: $0.7");
        this.coffee.setToppings(toppings);
        assertEquals(5.2, this.coffee.calculateTotalPrice(), TOLERANCE);
        assertEquals(toppings, this.coffee.getToppings());
    }

}

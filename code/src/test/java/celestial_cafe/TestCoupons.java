package celestial_cafe;
import celestial_cafe.CafeData.*;
import celestial_cafe.Products.*;
import celestial_cafe.Shopping.*;
import java.util.*;
import static org.junit.Assert.*;
import org.junit.Test;

public class TestCoupons {
    ICafeData cafe= new FileCafe();
    final List<MenuItem> MENU=cafe.loadProducts("..\\cafeFiles\\MenuFlavour.txt");
    
    private Drink tea = new Tea("Tea","Matcha", 5);
    private Drink coffee = new Coffee("Coffee","Cappuccino", 4);
    private List<MenuItem> order= new ArrayList<MenuItem>(Arrays.asList(tea, coffee));
    private Coupon fixed=new SetAmount();
    private Coupon percent=new Percentage();
    private Coupon combo=new Combos();
    private double TOLERANCE=0.00001;


    @Test
    public void testConstructor(){
        assertEquals("set amount", fixed.getType());
        assertEquals("percentage", percent.getType());
        assertEquals("Buy one, get one!", combo.getType());
    }


    @Test
    public void testIsApplicable(){
        assertTrue(fixed.isApplicable(MENU));
        assertTrue(percent.isApplicable(MENU));
        assertTrue(combo.isApplicable(MENU)); //may not work for duplicate values
    }
    //these change with each instance so you can't reliably add an expected field. 
    //Still can be used for testing, but will technically fail most of the time.
    @Test
    public void testCalculateDiscount(){
        //assertEquals(1.5, percent.calculateDiscount(), TOLERANCE);
        //assertEquals(3, fixed.calculateDiscount(), TOLERANCE);
        //assertEquals(3, combo.calculateDiscount(), TOLERANCE);
    }
    //similar to above, can be used for tests but values cannot be predicted
    @Test
    public void testToString(){
        //fixed.isApplicable(MENU);
        //assertEquals("",percent.toString());
        //assertEquals("",fixed.toString());
        //assertEquals("",combo.toString());
    }
    @Test
    public void testSetDiscountItem(){
        MenuItem discountItem = MENU.get(0);
        assertEquals(" Americano", discountItem.getFlavour());

        //checking if file loading works from there
        Coupon coupon = new Percentage();
        coupon.setRandomDiscountItem();
    }
    @Test
    public void testGetValue(){
        fixed.isApplicable(this.MENU);
        fixed.calculateDiscount();
        percent.calculateDiscount();
        combo.calculateDiscount();
        assertTrue(this.fixed.getValue()!=0);
        assertTrue(this.percent.getValue()!=0);
        assertTrue(this.combo.getValue()!=0);
    }

}
